sudo apt-get install linux-tools-common libqt5webkit-dev qt5-default
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable --ruby
source /home/bizneocr/.rvm/scripts/rvm
gem install bundle

#wireshark
sudo apt-get install wireshark
#Emacs
sudo apt-get install emacs
#Configure emacs 
#Install yasnipets 
#install flymake of Flycheck
#Config theme for emacs

echo "now you can instal all your gems using bundle install in the root of your project folder.
echo "Don't forget that you need to define a Gemfile.rb file!!


